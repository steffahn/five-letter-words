Faster implementation for problem described in [this YouTube video](https://www.youtube.com/watch?v=_-AfhLQfb6w).

To run this, [install rust](https://www.rust-lang.org/tools/install), then you can build it with `cargo build --release`, and run the program with:

* `cargo run --release -- wordle` using the Wordle word set, or
* `cargo run --release -- alpha` using the (larger) other word set.

On my 10 years old laptop, the `wordle` version finishes in under 4s,
and the `alpha` variant in under 6s. _(Update: The numbers should be even better now,
probably at least 4x better, but I haven't tested on the same machine again yet.)_

---

<details><summary><i>(I provide this code under the MIT license. [Click for licence text.])</i></summary>

```
Copyright 2022 Frank Steffahn

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS ORIMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```

</details>

---

### Explanation

##### Step 0: Encoding Words
The implementation starts out with the list of words, splitting it at line-breaks, naturally.

The first step is to encode the set of letters for each word in an integer.
The main idea here is to encode these sets efficiently and compactly using the bits in
an integer. For example, the word "abcde" (not a word) would have the set of characters
{'a', 'b', 'c', 'd', 'e'}, and this would be encoded as 00000000000000000000011111, 
a 26-bit long integer, where the last bit stands for 'a', the second-to-last stands
for 'b', and so on. The computer has 32-bit integers, so we'll add some more leading
zeros. E.g. 'horse' would be
```
00000000 00000110 01000000 10010000
^^^^^^        sr   o       h  e
    |
    +-- leading zeros
```

While computing this representation, we also discard words with duplicate letters.

This process represents anagrams as the same word. We build a table that saves,
for every word encoded in this way, the list of corresponding strings (either one
string, or multiple in case there's anagrams).

##### Step 1: Find the character sets

This integer-representation of sets cannot only represent the set of characters of
a single word, but also of multiple words. We start with a list of all character-sets
for a single word, and next search for character-sets of 2-word-pairs without any
letters in common.

E.g. for "fjord" and "waltz"
```
00000000 00000010 01000010 00101000
......         r   o    j    f d
```
```
00000010 01001000 00001000 00000001
......z   w  t        l           a
```
the combined set, {'a', 'd', 'f', 'j', 'l', 'o', 'r', 't', 'w', 'z'}, is represented as
```
00000010 01001010 01001010 00101001
......z   w  t r   o  l j    f d  a
```

Starting out with the two integers, we can get the resulting one by bit-wise `or` operation
and we can check whether or not there was overlap by checking if the result of bit-wise `and`
operation returns zero.

Listing all the character-sets of 2-word-pairs is thus achieved by iterating over all the 1-word
character-sets, and for each on, searching through all 1-word character sets, looking at
the bitwise-`and` whether its zero, and if it is, considering the bitwise-`or` result (otherwise
this combination is discarded).

This works analogously for larger tuples. All the 3-word character sets can be found
by iterating through all the 2-word-character sets, and for each such set, considering
all the 1-word-character sets and combining them if possible. The 4-word character sets
can be obtained by iterating over the 3-word character sets, and for each such set, considering
all the 1-word-character sets and combining them if possible. And so on.

We can even start the process with a list of 0-word character sets, containing the single set
encoded as `00000000 00000000 00000000 00000000`.

After each step, we deduplicate these lists (by sorting them and then removing duplicates).
It is also possible to optimize this process further by giving a canonical order to the
words:

I.e. if words "fjord" and "waltz" should form the set
{'a', 'd', 'f', 'j', 'l', 'o', 'r', 't', 'w', 'z'}, in order to find this set,
it's sufficient if, say, only for the set of "fjord", the word "waltz" was
considered to be added (if possible), but not vice versa, thus skipping the redundant
duplicate creation if this set.

The way we can order these sets is as-if they're numbers, so the most-significant
bit counts the most, which is the highest letter in the alphabet contained within
the set. "waltz" contains a "z", so it would come after "fjord" which only contains
an "r". In fact, because duplicate letters aren't allowed, starting with the "fjord" set
```
00000000 00000010 01000010 00101000
......         r   o    j    f d
```
in order to extend it to 2-word sets, we can start out with sets that contain an
's' or higher letter, so we only consider sets whose numerical encoding is larger
than or equal to
```
00000000 00000100 00000000 00000000
......        s
```

Since for deduplication purposes, the 1-word sets list is sorted already anyways,
this amounts to a simple binary-search for the starting point.

##### Step 2: Prune the "tree"

After this process e.g. in the wordle list, the last step gives us 3 5-word character
sets:
```
00000011 01111111 11111111 11111111
......zy  wvutsrq ponmlkji hgfedcba
00000011 11111110 11111111 11111111
......zy xwvutsr  ponmlkji hgfedcba
00000011 11111111 11111101 11111111
......zy xwvutsrq ponmlk i hgfedcba
```
missing the letter `x`, `q`, or `j`. This alone doesn't help a lot with giving
back a concrete list of words, but it's important that we *also* have the list
of 4-word character sets, the 3-word ones, and the 2-word ones. This gives us
effectively a representation of the search-tree we were following. (Or perhaps
rather the search-DAG [directed acyclic graph]); each node is an *n*-word character
set, and an edge connects any given *n*-word set to a given target *n+1*-word one if and only if
there exists a 1-word set such that the bitwise `and` of the given *n*-word set
and that 1-word set creates the given *n+1*-word set.

We can re-trance our steps along this search-"tree" to reconstruct the actual
lists of words. This search-"tree" has lots of lose ends still though: nodes
that don't have any directed paths to any 5-word character sets. With these
loose ends, searching along the "tree" would re-visit all the choices we made
while creating it. But we can do better, and *prune* this "tree" to remove all
lose ends.

We start with the 4-word sets, and remove all of them that aren't connected
to any of the 5-word sets. To do this, for each set in the 4-words character sets, we will
consider all of the 1-word sets, and see if this 1-word set constitutes an edge
to one of the 5-word sets: First see if the 1-word set has no overlap (bitwise
`or` equals zero), then look up the result of bitwise `and` in the list of
5-word sets (via binary search).

Then continue with the 3-word sets, removing the ones not connected to any *remaining*
4-word set, and so on.

We do it this way (iterating over all the
1-word sets to find a matching *n+1*-word set for our given *n*-word set,
*not* iterating over all *n+1*-word set for any given *n*-word set)
because in general there can be a lot fewer 1-word sets than there
are remaining *n+1*-word sets.

###### Optimization

A useful optimization of this pruning step is to reduce the 1-word character sets
to the ones we're actually using, before the pruning is started. A 1-word character
set is *used* (i.e. appears in any of the solution sets) if and only if it
constitutes an edge between one of the 4-word sets and one of the 5-word sets.

##### Step 3: Finding actual lists of 5 1-word character sets

With the pruned "tree" (or DAG, actually), we can recursively search along this tree
while keeping track of the 1-word character sets along the edges we follow.
(Instead of a recursive depth-first traversal, a breadth-first approach would
also appear reasonable, but this step is quite fast either way). This "search"
is more of an enumeration of possibilities anyway, because we have pruned away
all opportunities of failure (the "lose ends" in the previous steps); so if this
recursive traversal were to take a long time then only because there'd be a very
large number of solutions.

##### Step 4: Finding word lists

The previous step would only give us lists of 5 matching 1-word character sets
(encoded as integers). We still have to look up these in the table from the
preprocessing step (Step 0) to be able to display the actual words.

Step 0 produced a list of `(character set, list of strings)` pairs (the
character sets are integers) that is sorted by the (encoded) set, so
looking up values via binary-search will be quite fast.

We don't bother factoring out the anagrams here, so solutions are displayed
as-is by printing the whole anagram-list of any given character sets, e.g.
```
Solution #8:
["kempt"]
["brung"]
["waqfs"]
["xylic", "cylix"]
["vozhd"]
```
