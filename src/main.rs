use std::{
    env::args,
    io::{stdout, Write},
    mem,
};

use rayon::prelude::*;

use arrayvec::ArrayVec;
use itertools::Itertools;
use smallvec::SmallVec;

type Instances = SmallVec<[&'static str; 3]>;

const GUESSES: &str = include_str!("../wordle-allowed-guesses.txt");
const ANSWERS: &str = include_str!("../wordle-answers-alphabetical.txt");
const ALL: &str = include_str!("../words_alpha.txt");

#[cfg(debug_assertions)]
const DEBUG_WARNING: &str = "\
Debug assertions are active, indicating that
this program might not be compiled in
`--release` mode. Make sure to enable opti-
mizations if you are timing the execution
of this program!
";

fn main() {
    #[cfg(debug_assertions)]
    {
        println!("\n{}", DEBUG_WARNING);
    }

    let args = args().collect_vec();
    if args.len() != 2 || (args[1] != "alpha" && args[1] != "wordle") {
        println!("provide one argument: 'alpha' or 'wordle' (no quotation marks)");
        return;
    }

    println!("Reading words...");

    let mut words = if args[1] == "wordle" {
        let words = words_in(GUESSES).chain(words_in(ANSWERS));

        words
            .filter_map(|w| Some((word(w)?, Instances::from_iter([w]))))
            .collect_vec()
    } else {
        let words = words_in(ALL).filter(|w| w.chars().count() == 5);

        words
            .filter_map(|w| Some((word(w)?, Instances::from_iter([w]))))
            .collect_vec()
    };

    dedup(&mut words, |w1, w2| w1.extend(mem::take(w2)));
    let all_words = words.iter().map(|tup| tup.0).collect_vec();

    println!("\nEnumerating possibilities (not counting anagrams across multiple words)...");
    let mut setss: Vec<Vec<_>> = vec![vec![0]];
    for i in 0..5 {
        let i = i + 1;
        print!("Length {i}...");
        stdout().flush().unwrap();
        let sets = step(setss.last().unwrap(), &all_words);
        println!("   found {:>8} possibilities!", sets.len());
        setss.push(sets)
    }
    setss.remove(0);
    let setss: &mut [_; 5] = (&mut setss[..]).try_into().unwrap();

    print!("\nReducing words set... ");
    stdout().flush().unwrap();
    let all_words = all_words
        .into_par_iter()
        .filter(|word| {
            setss[4]
                .iter()
                .any(|&set| (set | word == set) && setss[3].binary_search(&(set & !word)).is_ok())
        })
        .collect::<Vec<_>>();
    println!(
        "only {} out of {} words appear in the solutions!",
        all_words.len(),
        words.len()
    );

    prune(setss, &all_words);

    let mut solutions = vec![];
    println!("\nSearching sets of 5 words along the pruned tree...");
    find::<NS<NS<NS<NS<NS<NZ>>>>>>(setss, 0, &all_words, &mut ArrayVec::new(), &mut solutions);

    println!(
        "\nFound {} solutions (not counting anagrams of individual words)!",
        solutions.len()
    );
    let mut actual_total_number = 0_usize;
    for (i, s) in solutions.iter().enumerate() {
        let i = i + 1;
        println!("\nSolution #{i}:");
        let mut number = 1_usize;
        for word in s {
            let ix = words.binary_search_by_key(word, |w| w.0).unwrap();
            let anagrams = &words[ix].1;
            println!("{anagrams:?}");
            number *= anagrams.len();
        }
        actual_total_number += number;
    }
    println!("\nWithout deduplicating anagrams of individual words, we found {actual_total_number} solutions!\n");

    #[cfg(debug_assertions)]
    {
        println!("{}", DEBUG_WARNING);
    }
}

fn words_in(c: &str) -> impl Iterator<Item = &str> {
    c.trim().lines().map(|l| l.trim())
}

fn prune(setss: &mut [Vec<u32>; 5], all_words: &[u32]) {
    println!("\nPruning away any letter sets that are unsuccessfull down the line...");
    println!(
        "Length 5: {:>8} possibilities (starting point, these aren't pruned)",
        setss[4].len()
    );
    for i in (0..4).rev() {
        print!(
            "Length {}: {:>8} possibilities are pruned...",
            i + 1,
            setss[i].len()
        );
        stdout().flush().unwrap();
        match &mut setss[i..i + 2] {
            [sets1, sets2] => {
                *sets1 = mem::take(sets1)
                    .into_par_iter()
                    .filter(|pat| {
                        all_words
                            .iter()
                            .any(|w| w & pat == 0 && sets2.binary_search(&(w | pat)).is_ok())
                    })
                    .collect();
            }
            _ => unreachable!(),
        }
        println!("  to {:>4} possibilities!", setss[i].len());
    }
}

trait Nat {
    const IS_ZERO: bool;
    const VAL: usize;
    type Pred: Nat;
}
struct NZ;
impl Nat for NZ {
    const IS_ZERO: bool = true;
    const VAL: usize = 0;
    type Pred = NZ;
}
struct NS<T>(T);
impl<T: Nat> Nat for NS<T> {
    const IS_ZERO: bool = false;
    const VAL: usize = T::VAL + 1;
    type Pred = T;
}

fn find<T: Nat>(
    guide: &[Vec<u32>; 5],
    pat: u32,
    ws: &[u32],
    partial: &mut ArrayVec<u32, 5>,
    solutions: &mut Vec<[u32; 5]>,
) {
    if T::IS_ZERO {
        solutions.push(partial.clone().into_inner().unwrap());
        return;
    }
    for (i, &w) in ws.iter().enumerate() {
        if pat & w == 0 && guide[5 - T::VAL].binary_search(&(pat | w)).is_ok() {
            partial.push(w);
            find::<T::Pred>(guide, pat | w, &ws[i + 1..], partial, solutions);
            partial.pop();
        }
    }
}

fn char_ix(c: char) -> u32 {
    let r = c as u32 - 'a' as u32;
    assert!(r <= 26);
    r
}

fn word(w: &str) -> Option<u32> {
    let mut word = 0_u32;
    let (a, b, c, d, e) = w.chars().collect_tuple().unwrap();
    for letter in [a, b, c, d, e] {
        let mask = 1_u32 << char_ix(letter);
        if mask & word > 0 {
            return None;
        }
        word |= mask;
    }
    Some(word)
}

fn dedup<T>(words: &mut Vec<(u32, T)>, mut combine: impl FnMut(&mut T, &mut T)) {
    words.sort_unstable_by_key(|w| w.0);
    words.dedup_by(|w1, w2| {
        let same = w1.0 == w2.0;
        if same {
            combine(&mut w2.1, &mut w1.1) // opposite order!
        }
        same
    });
}

fn step(collection: &[u32], all_words: &[u32]) -> Vec<u32> {
    let mut new_coll = collection
        .par_iter()
        .copied()
        .flat_map_iter(|words| {
            let zs = words.leading_zeros();
            // 0000001101 - example with 10 bits (not 32), 6 leading zeroes. We want:
            // 0000010000 - which is 1 << 4, and 4 = 10 - 6.
            let first = 1_u32 << (32 - zs);
            let (Ok(i) | Err(i)) = all_words.binary_search(&first);
            all_words[i..]
                .iter()
                .copied()
                .filter(move |pat| pat & words == 0)
                .map(move |pat| pat | words)
        })
        .collect::<Vec<_>>();
    new_coll.sort_unstable();
    new_coll.dedup();
    new_coll
}
